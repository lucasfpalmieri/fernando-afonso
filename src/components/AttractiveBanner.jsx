import React from 'react';

const AttractiveBanner = () => {
    return (
        <div style={{backgroundColor: "#FFB403", padding: "20px"}}>
            <p style={{fontSize: "26px", color: "white"}}>Fale Conosco</p>
            <p style={{fontSize: "24px", color: "white", margin: 0}}>11 96688-1555</p>
            <div>
                <p style={{fontSize: "12px"}} className="text-white mb-0">Rua Correia de Lemos, 543 cj. 111</p>
                <p style={{fontSize: "12px"}} className="text-white">São Paulo, SP, 04140-000</p>
            </div>
            <div>
                <p style={{color: "#404144", margin: 0}}>EMAIL</p>
                <p className="text-white">fernandoafonso@advogado.com</p>
            </div>
        </div>
    );
}

export default AttractiveBanner;