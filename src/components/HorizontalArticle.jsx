import React from 'react';
import { Link } from 'react-router-dom';

const HorizontalArticle = (props) => {
    const txtStyle = {
        fontSize: "13.5px",
    }
    return (
        <div className="d-flex align-items-top justify-content-between mt-3">
            <div>
                <img style={{width: "134px", height: "134px", marginRight: "15px"}} src={props.img} alt={props.alt} />
            </div>
            <div>
                <p className="m-0" style={txtStyle}>{props.date}</p>
                <p style={txtStyle}>{props.text}</p>
                <Link to="servicos" className="text-warning" style={txtStyle}>LEIA MAIS</Link>
            </div>
        </div>
    )
}

export default HorizontalArticle;