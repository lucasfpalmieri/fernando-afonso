import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import ArticleTitle from './ArticleTitle';

const VerticalArticle = props => {
  return (
    <div>
      <ArticleTitle text={props.title} />
      <p style={{ color: "#404144" }}>{props.text}</p>
      {props.button && (
        <Link to={props.link}>
          <Button>Mais</Button>
        </Link>
      )}
    </div>
  );
};

const Button = styled.button`
  background: #404144;
  color: white;
  border: none;
  border-radius: 5px;
  width: 80px;
  padding: 5px 0;
  cursor: pointer;

  &:hover {
    background: #525356
  }
`;

export default VerticalArticle;
