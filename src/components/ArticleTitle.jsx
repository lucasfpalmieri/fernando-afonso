import React from "react";
import styled from "styled-components";

const ArticleTitle = props => {
  return <Title>{props.text}</Title>;
};

const Title = styled.h2`
  color: ${props => props.yellow ? "#ffb403" : "#404144"};
  border-top: 3px solid ${props => props.yellow ? "#ffb403" : "#404144"};
`;

export default ArticleTitle;
