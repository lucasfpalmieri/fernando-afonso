import React from "react";
import { NavLink } from "react-router-dom";
import styled from "styled-components";

import logo from "../img/logo.jpeg";

const Header = () => {
  return (
    <Nav className="container-fluid">
      <div className="container d-flex justify-content-between align-items-center">
        <div>
          <img style={{ width: "300px" }} src={logo} alt="Fernando Afonso" />
        </div>
        <div className="d-flex">
          <NavLink
						className="mr-3 text-warning"
						activeStyle={{
              fontWeight: "bold",
            }}
            to="/home"
          >
            Página inicial
          </NavLink>
          <NavLink
						className="mr-3 text-warning"
						activeStyle={{
              fontWeight: "bold",
            }}
            to="/escritorio"
          >
            Escritório
          </NavLink>
          <NavLink
						className="mr-3 text-warning"
						activeStyle={{
              fontWeight: "bold",
            }}
            to="equipe"
          >
            Advogados
          </NavLink>
          <NavLink
						className="mr-3 text-warning"
						activeStyle={{
              fontWeight: "bold",
            }}
            to="servicos"
          >
            Áreas de exercício
          </NavLink>
          <NavLink
						className="mr-3 text-warning"
						activeStyle={{
              fontWeight: "bold",
            }}
            to="contato"
          >
            Contato
          </NavLink>
        </div>
      </div>
    </Nav>
  );
};

const Nav = styled.header`
  background-color: #2d2d2f;
`;

// const NavLink = styled.div`
//   color: white;
//   margin-right: 10px;

//   &:hover {
//     color: #7ccaff;
//   }
// `;

export default Header;
