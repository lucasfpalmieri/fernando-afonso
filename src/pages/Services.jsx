import React from "react";
import VerticalArticle from "../components/VerticalArticle";
import { Row, Col } from "react-bootstrap";

const Services = () => {
  const articleTxt = `Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e você poderá adicionar seu conteúdo e trocar fontes. Você pode arrastar e soltar-me em qualquer lugar de sua página. Sou um ótimo lugar para contar sua história e permitir que seus visitantes saibam um pouco mais sobre você.`;

  return (
    <div className="container mt-4">
      <h2>Áreas de exercício</h2>
      <p>
        Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É
        fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e
        você poderá adicionar seu conteúdo e trocar fontes. Você pode arrastar e
        soltar-me em qualquer lugar de sua página. Sou um ótimo lugar para
        contar sua história e permitir que seus visitantes saibam um pouco mais
        sobre você.
      </p>
      <Row className="mt-5">
        <Col lg={4}>
          <VerticalArticle title="Direito trabalhista e previdenciário" text={articleTxt} />
        </Col>
        <Col lg={4}>
          <VerticalArticle title="Direito civil" text={articleTxt} />
        </Col>
        <Col lg={4}>
          <VerticalArticle title="Direito penal" text={articleTxt} />
        </Col>
      </Row>
      <Row className="mt-5">
        <Col lg={4}>
          <VerticalArticle title="Direito contratual" text={articleTxt} />
        </Col>
        <Col lg={4}>
          <VerticalArticle title="Direito tributário" text={articleTxt} />
        </Col>
        <Col lg={4}>
          <VerticalArticle title="Direito ambiental" text={articleTxt} />
        </Col>
      </Row>
    </div>
  );
};

export default Services;
