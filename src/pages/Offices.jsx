import React from "react";
import { Row, Col } from "react-bootstrap";

const Offices = props => {
    const txtStyles = {
        large: {
            fontSize: "24px"
        },
        small: {
            fontSize: "14px"
        },
    }
  return (
    <div className="container mt-5">
      <h2 className="text-dark">Nosso escritório</h2>
      <Row className="mt-3">
        <Col lg={8}>
          <p className="text-dark" style={txtStyles.large}>
            Sou um parágrafo. Clique aqui para me editar e adicionar seu texto.
            É fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre
            mim e você poderá adicionar seu conteúdo e trocar fontes.
          </p>
          <p className="text-dark" style={txtStyles.small}>
            Você pode arrastar e soltar-me em qualquer lugar de sua página. Sou
            um ótimo lugar para contar sua história e permitir que seus
            visitantes saibam um pouco mais sobre você.
          </p>
          <p className="text-dark" style={txtStyles.small}>
            Este é um ótimo espaço para escrever um texto mais longo sobre sua
            empresa e seus serviços. Você pode usar este espaço para contar mais
            detalhes sobre sua empresa. Fale sobre sua equipe e os serviços
            prestados por você. Conte aos seus visitantes como teve a ideia de
            iniciar o seu negócio e o que o torna diferente de seus
            concorrentes. Faça com que sua empresa se destaque e mostre quem
            você é. Dica: Adicione a sua própria imagem clicando duas vezes
            sobre a imagem e em Trocar imagem.
          </p>
          <p className="text-dark" style={txtStyles.small}>
            No Wix, amamos criar templates que lhe permitem construir sites
            incríveis! Tudo isso graças ao feedback e apoio de usuários como
            você! Mantenha-se atualizado sobre novos recursos na seção Sobre
            Wix, na página de Suporte. Sinta-se à vontade para dar sua opinião e
            feedback no Fórum Wix Responde. Se quiser se beneficiar de um toque
            de um designer profissional, vá ao Arena Wix e contate um dos
            designers Wix Pro. Ou, se precisar de mais ajuda, pode digitar suas
            perguntas no Fórum de Suporte e receber respostas imediatas. Para
            estar sempre atualizado com tudo que acontece no Wix, incluindo
            dicas e coisas legais, entre no Wix Blog!
          </p>
          <p className="text-dark" style={txtStyles.small}>
            Dica: Use esta área para descrever um de seus serviços. Você pode
            trocar o título para o serviço que você oferece e usar esta área de
            texto para descrever seu serviço.
          </p>
        </Col>
        <Col lg={4}>
            <img className="w-100" src="https://images.unsplash.com/photo-1527259216948-b0c66d6fc31f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80" alt="office" />
        </Col>
      </Row>
    </div>
  );
};

export default Offices;
