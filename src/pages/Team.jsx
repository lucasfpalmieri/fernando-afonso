import React from "react";

const Team = () => {
  return (
    <div className="container">
      <h1 className="text-center mt-3">Fernando Afonso</h1>
      <div style={{ width: "294px", margin: "auto" }}>
        <img
          className="w-100 mb-3"
          src="https://images.unsplash.com/photo-1507679799987-c73779587ccf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1651&q=80"
          alt="Fernando Afonso"
        />
        <p>
          Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É
          fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e
          você poderá adicionar seu conteúdo e trocar fontes. Você pode arrastar
          e soltar-me em qualquer lugar de sua página. Sou um ótimo lugar para
          contar sua história e permitir que seus visitantes saibam um pouco
          mais sobre você.
        </p>
        <p>
          Este é um ótimo espaço para escrever um texto mais longo sobre sua
          empresa e seus serviços. Você pode usar este espaço para contar mais
          detalhes sobre sua empresa.
        </p>
      </div>
    </div>
  );
};

export default Team;
