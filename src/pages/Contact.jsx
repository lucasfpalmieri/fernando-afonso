import React from "react";
import { Row, Col } from "react-bootstrap";
import styled from 'styled-components';
import ArticleTitle from "../components/ArticleTitle";

const Contact = () => {
  return (
    <div className="container">
      <h2 className="my-4">Contato</h2>
      <Row>
        <Col lg={8}>
          <ArticleTitle text="Endereço" />
          <p className="mb-0">Rua Correia de Lemos, 543 cj. 111</p>
          <p className="mb-0">São Paulo, SP</p>
          <p className="mb-0">04140-000</p>
          <p>fernandoafonso@advogado.com</p>
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.9158420329804!2d-46.63465188514956!3d-23.607350984660062!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5a4b2f590249%3A0x9341f41bede8e497!2sCondom%C3%ADnio%20Edif%20Monte%20Alverne%20-%20Rua%20Correia%20de%20Lemos%2C%20543%20-%20Ch%C3%A1cara%20Inglesa%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2004140-000!5e0!3m2!1sen!2sbr!4v1592350695082!5m2!1sen!2sbr"
            width="600"
            height="450"
            title="myFrame"
            frameborder="0"
            style={{ border: 0 }}
            allowfullscreen=""
            aria-hidden="false"
            tabindex="0"
          />
        </Col>
        <Col lg={4}>
          <ArticleTitle text="Telefone" />
          <p>(11) 96688-1555</p>
          <form>
              <input type="text" placeholder="Nome" className="d-block w-100 mb-2"/>
              <input type="email" placeholder="Email" className="d-block w-100 mb-2"/>
              <input type="text" placeholder="Telefone" className="d-block w-100 mb-2"/>
              <textarea name="message" id="message" cols="30" rows="10" className="d-block w-100 mb-2"></textarea>
              <Button type="submit">Enviar</Button>
          </form>
        </Col>
      </Row>
    </div>
  );
};

const Button = styled.button`
    background: #ffb403;
    color: white;
    border: none;
    border-radius: 5px;
    width: 111px;
    display: block;
    margin-left: auto;
`;

export default Contact;
