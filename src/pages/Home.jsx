import React from "react";
import { Row, Col, Carousel } from "react-bootstrap";
import meeting from "../img/meeting.jpg";
import VerticalArticle from "../components/VerticalArticle";
import AttractiveBanner from "../components/AttractiveBanner";
import HorizontalArticle from "../components/HorizontalArticle";
import ArticleTitle from  "../components/ArticleTitle";

const Home = () => {
  let teamText = `Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e você poderá adicionar seu conteúdo e trocar fontes. Este é um ótimo espaço para escrever um texto mais longo sobre sua empresa e seus serviços. Você pode usar este espaço para contar mais detalhes sobre sua empresa.`;

  let ArticleText = `Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e você poderá adicionar seu conteúdo e trocar fontes. Você pode arrastar e soltar-me em qualquer lugar de sua página. Sou um ótimo lugar para contar sua história e permitir que seus visitantes saibam um pouco mais sobre você.`;

  return (
    <div className="container">
      <Carousel
        className="d-flex m-auto"
        controls={false}
        indicators={false}
        style={{ height: "380px" }}
      >
        <Carousel.Item>
          <img className="d-block w-100" src={meeting} alt="First slide" />
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={meeting} alt="Third slide" />
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={meeting} alt="Third slide" />
        </Carousel.Item>
      </Carousel>
      <Row className="mt-4">
        <Col lg={8}>
          <Row>
            <Col lg={6}>
              <VerticalArticle
                title="Nossa equipe"
                text={teamText}
                button={true}
                link="equipe"
              />
            </Col>
            <Col lg={6}>
              <VerticalArticle
                title="Serviços"
                text={teamText}
                button={true}
                link="servicos"
              />
            </Col>
          </Row>
          <div className="mt-5">
            <ArticleTitle text="Notícias e publicaçõe" />
            <HorizontalArticle img="https://images.unsplash.com/photo-1479142506502-19b3a3b7ff33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2850&q=80" alt="default" date="25/10/2023" text={ArticleText} />
            <HorizontalArticle img="https://images.unsplash.com/photo-1479142506502-19b3a3b7ff33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2850&q=80" alt="default" date="25/10/2023" text={ArticleText} />
          </div>
        </Col>
        <Col lg={4}>
          <AttractiveBanner />
        </Col>
      </Row>
    </div>
  );
};

export default Home;
