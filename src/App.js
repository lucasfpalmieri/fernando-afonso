import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./components/Header";
import Home from "./pages/Home";
import Offices from "./pages/Offices";
import Team from "./pages/Team";
import Services from "./pages/Services";
import Contact from "./pages/Contact";

function App() {
  return (
    <>
      <Router>
        <Header />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/escritorio">
            <Offices />
          </Route>
          <Route path="/equipe">
            <Team />
          </Route>
          <Route path="/servicos">
            <Services />
          </Route>
          <Route path="/contato">
            <Contact />
          </Route>
        </Switch>
      </Router>
    </>
  );
}

export default App;
